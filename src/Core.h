#ifndef CORE_H
#define CORE_H

#include <vector>
#include <SFML/Graphics.hpp>


#include "Grid.h"

class Core
{
    public:

        static sf::RenderWindow window;
        static std::vector<Grid> grids;

        static int frame;


        static void run();
        static void yield();
};

#endif // CORE_H
