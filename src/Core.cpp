#include "Core.h"

sf::RenderWindow Core::window(
    //sf::VideoMode::getDesktopMode(),
    sf::VideoMode(1000, 1000),
    "A* vs Dijkstra",
    sf::Style::Default);

std::vector<Grid> Core::grids;

int Core::frame = 0;

void Core::run(){
    //window.setVerticalSyncEnabled(true);

    grids.push_back(Grid(sf::Vector2i(300,300),sf::Color::White,3,0,sf::Color::Black));

    while(window.isOpen()){
        sf::Event event;
        while (window.pollEvent(event)){
            if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Keyboard::isKeyPressed(sf::Keyboard::F12)){
                window.close();
            }
        }
        window.clear();

        Core::yield();

        window.display();


    }
}

void Core::yield(){
    if(frame == 0){
        for(;frame<90000;++frame){
            grids[0].setCellColor(
                sf::Vector2i(frame%300,frame%90000/300),
                sf::Color(
                    frame*24%256,         //
                    (frame*27+64)%256,    // this is fun
                    (frame*29+192)%256,   //
                    255
                )
            );
        }
    }
    window.draw(grids[0]);
}
