#include "Grid.h"

Grid::Grid(sf::Vector2i gridSize, sf::Color defaultCellColor, int cellSize, int cellSpacing, sf::Color bgColor):
gridSize(gridSize),
cellSize(cellSize),
cellSpacing(cellSpacing),
bgColor(bgColor){
    grid.setPrimitiveType(sf::Quads);

    resize(gridSize);
    setAllCellsColor(defaultCellColor);
    rearrangeCells();
}

void Grid::resize(sf::Vector2i gridSize){
    this->gridSize = gridSize;
    grid.resize(gridSize.x * gridSize.y * 4);
}

void Grid::setCellColor(sf::Vector2i cellPos,sf::Color color){
    // gets the correct quad and then apply the color to all its edges
    sf::Vertex* quad = &grid[(cellPos.x *  gridSize.x + cellPos.y) * 4];
    for(unsigned i=0;i<4;++i){
        quad[i].color = color;
    }
}

void Grid::rearrangeCells(){
    sf::Vertex* quad = nullptr;
    sf::Vector2i cellPos = sf::Vector2i(0,0);
    for(unsigned x=0;x<gridSize.x;++x){
        cellPos.x = cellSpacing + (cellSpacing + cellSize) * x; // cache the topleft point X coordinate for the current quad
        for(unsigned y=0;y<gridSize.y;++y){
            cellPos.y = cellSpacing + (cellSpacing + cellSize) * y; // cache the topleft point Y coordinate for the current quad
            quad = &grid[(x *  gridSize.x + y) * 4];

            quad[0].position = sf::Vector2f(cellPos.x,cellPos.y); // topleft
            quad[1].position = sf::Vector2f(cellPos.x + cellSize,cellPos.y); // topright
            quad[2].position = sf::Vector2f(cellPos.x + cellSize,cellPos.y + cellSize); // bottomright
            quad[3].position = sf::Vector2f(cellPos.x,cellPos.y + cellSize); // bottomleft
        }
    }
}

void Grid::setAllCellsColor(sf::Color color){
    for(unsigned x=0;x<gridSize.x;++x){
        for(unsigned y=0;y<gridSize.y;++y){
            setCellColor(sf::Vector2i(x,y),color);
        }
    }
}
