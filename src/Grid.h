#ifndef GRID_H
#define GRID_H

#include <SFML/Graphics.hpp>

class Grid : public sf::Drawable, public sf::Transformable
{
    public:
        Grid(sf::Vector2i gridSize, sf::Color defaultCellColor, int cellSize, int cellSpacing, sf::Color bgColor); // builder

        void resize(sf::Vector2i gridSize); // give a new size for our grid

        void setCellColor(sf::Vector2i cellPos,sf::Color color); // sets the color for a cell

        void setAllCellsColor(sf::Color color); // sets the color for all cells


    protected:

        void rearrangeCells(); // rebuilds the vertex array (doesn't change color) , to be used when cell size, spacing or gridsize is modified

        sf::Vector2i gridSize; // grid size : x,y

        int cellSize; // the size of the square representing a cell
        int cellSpacing; // the space between cells

        sf::Color bgColor; // background color

        sf::VertexArray grid; // the grid itself

    private:
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const{ // draw method for sfml
        states.transform *= getTransform();
        states.texture = NULL;
        target.draw(grid, states);
    }

};

#endif // GRID_H
